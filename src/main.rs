// nop-preprocessors.rs

use crate::davids_cooking::DavidsCooking;
use clap::{crate_version, Arg, ArgMatches, Command};
use mdbook::book::{Book, BookItem};
use mdbook::errors::Error;
use mdbook::preprocess::{CmdPreprocessor, Preprocessor, PreprocessorContext};
use regex::Regex;
use semver::{Version, VersionReq};
use std::io;
use std::process;

pub fn make_app() -> Command {
    Command::new("davids_cooking")
        .about("A mdbook preprocessor that helps david create mini tables for steps in recipes")
        .subcommand(
            Command::new("supports")
                .arg(Arg::new("renderer").required(true))
                .about("Check whether a renderer is supported by this preprocessor"),
        )
        .version(crate_version!())
}

fn main() {
    let matches = make_app().get_matches();

    // Users will want to construct their own preprocessor here
    let preprocessor = DavidsCooking::new();

    if let Some(sub_args) = matches.subcommand_matches("supports") {
        handle_supports(&preprocessor, sub_args);
    } else if let Err(e) = handle_preprocessing(&preprocessor) {
        eprintln!("{}", e);
        process::exit(1);
    }
}

fn handle_preprocessing(pre: &dyn Preprocessor) -> Result<(), Error> {
    let (ctx, book) = CmdPreprocessor::parse_input(io::stdin())?;

    let book_version = Version::parse(&ctx.mdbook_version)?;
    let version_req = VersionReq::parse(mdbook::MDBOOK_VERSION)?;

    if !version_req.matches(&book_version) {
        eprintln!(
            "Warning: The {} plugin was built against version {} of mdbook, \
             but we're being called from version {}",
            pre.name(),
            mdbook::MDBOOK_VERSION,
            ctx.mdbook_version
        );
    }

    let processed_book = pre.run(&ctx, book)?;
    serde_json::to_writer(io::stdout(), &processed_book)?;

    Ok(())
}

fn handle_supports(pre: &dyn Preprocessor, sub_args: &ArgMatches) -> ! {
    let renderer = sub_args
        .get_one::<String>("renderer")
        .expect("Required argument");
    let supported = pre.supports_renderer(renderer);

    // Signal whether the renderer is supported by exiting with 1 or 0.
    if supported {
        process::exit(0);
    } else {
        process::exit(1);
    }
}

/// The actual implementation of the `Nop` preprocessor. This would usually go
/// in your main `lib.rs` file.
mod davids_cooking {
    use super::*;

    /// A no-op preprocessor.
    pub struct DavidsCooking;

    impl DavidsCooking {
        pub fn new() -> DavidsCooking {
            DavidsCooking
        }
    }

    impl Preprocessor for DavidsCooking {
        fn name(&self) -> &str {
            "davids_cooking"
        }

        fn run(&self, ctx: &PreprocessorContext, mut book: Book) -> Result<Book, Error> {
            // In testing we want to tell the preprocessor to blow up by setting a
            // particular config value
            if let Some(nop_cfg) = ctx.config.get_preprocessor(self.name()) {
                if nop_cfg.contains_key("blow-up") {
                    anyhow::bail!("Boom!!1!");
                }
            }
            let re = Regex::new(
                r"<(S|s)tep (?<vel>\d+) (?<time>\d+) (?<time_unit>min|sec) (?<temp>\d+)/>",
            )
            .unwrap();
            book.for_each_mut(|section| {
                if let BookItem::Chapter(ch) = section {
                    ch.content = re
                        .replace_all(
                            &ch.content,
                            "<div class=\"table-wrapper\"><table><thead><tr><th> v </th><th> t </th><th> temp </th></tr></thead><tbody><tr><td> $vel </td><td> $time $time_unit  </td><td>  $temp ºC </td></tr></tbody></table></div>",
                        )
                        .to_string();
                }
            });

            // we *are* a no-op preprocessor after all
            Ok(book)
        }

        fn supports_renderer(&self, renderer: &str) -> bool {
            renderer != "not-supported"
        }
    }

    #[cfg(test)]
    mod test {
        use super::*;

        #[test]
        fn nop_preprocessor_run() {
            let input_json = r##"[
                {
                    "root": "/path/to/book",
                    "config": {
                        "book": {
                            "authors": ["AUTHOR"],
                            "language": "en",
                            "multilingual": false,
                            "src": "src",
                            "title": "TITLE"
                        },
                        "preprocessor": {
                            "nop": {}
                        }
                    },
                    "renderer": "html",
                    "mdbook_version": "0.4.21"
                },
                {
                    "sections": [
                        {
                            "Chapter": {
                                "name": "Chapter 1",
                                "content": "# Chapter 1<step 4 4 min 110/>\n",
                                "number": [1],
                                "sub_items": [],
                                "path": "chapter_1.md",
                                "source_path": "chapter_1.md",
                                "parent_names": []
                            }
                        }
                    ],
                    "__non_exhaustive": null
                }
            ]"##;
            let input_json = input_json.as_bytes();

            let (ctx, book) = mdbook::preprocess::CmdPreprocessor::parse_input(input_json).unwrap();
            let result = DavidsCooking::new().run(&ctx, book);
            assert!(result.is_ok());

            // The nop-preprocessor should not have made any changes to the book content
            let actual_book = result.unwrap();
            match actual_book.sections.first().unwrap() {
                BookItem::Chapter(ch) => {
                    assert_eq!(
                        &ch.content,
                        "# Chapter 1<div class=\"table-wrapper\"><table><thead><tr><th> v </th><th> t </th><th> temp </th></tr></thead><tbody><tr><td> 4 </td><td> 4 min  </td><td>  110 ºC </td></tr></tbody></table></div>\n"
                    )
                }
                _ => assert!(false),
            }
        }
    }
}
